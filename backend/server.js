const express = require("express");
const cors = require("cors");
const appRoute = require("./routes/Routes");

const app = express();

app.use(cors("*"));

app.use(express.json());

app.use("/app", appRoute);

app.listen(4000, () => {
  console.log("Server started on port 4000");
});
