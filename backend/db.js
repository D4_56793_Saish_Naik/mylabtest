const mysql = require("mysql2");

const pool = mysql.createPool({
  host: "mysql",
  user: "root",
  password: "root",
  database: "examdb",
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

module.exports = pool;
